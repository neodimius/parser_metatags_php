function validate() {
    var url = document.getElementById("url").value;
    var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    if (pattern.test(url)) {
        document.getElementById("error").innerHTML = "Url is valid";
        $("#parseBtn").show();
        return true;
    } else {
        document.getElementById("error").innerHTML = "Url is invalid";
        $("#parseBtn").hide();
        return false;
    }
}
function parsing() {
    var url = document.getElementById("url").value;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("out").innerHTML = this.responseText;
        }
    };

    xhttp.open("POST", "model/parsing.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("url="+url);
}