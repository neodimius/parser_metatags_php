<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Drupal</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="wrapper">
        <div id = "headerMobile">
            <button id="btnMobile">
                <div></div>
                <div></div>
            </button>
            <div id="menuMobile">
                <div>
                    <span><a href="index.php?page=home">Home</a></span>
                    <span><a href="index.php?page=home">Features</a></span>
                    <span><a href="index.php?page=home">About</a></span>
                    <span><a href="index.php?page=home">LogIn</a></span>
                    <span><a href="classes/parse.php">Parsing</a></span>
                    <?php if ($_GET['all']) include "view/link.php"; //Link to show all Results?>
                </div>
            </div>
            <div><img src="img/headerMobile.jpg" alt="headerMobile"></div>
        </div>

        <header>
          <div>
            <nav>
              <img src="img/logo.png" srcset="img/logo@2x.png 2x, img/logo@3x.png 3x" class="Logo">
              <span><a href="index.php?page=home">Home</a></span>
              <span><a href="index.php?page=home">Features</a></span>
              <span><a href="index.php?page=home">About</a></span>
              <span><a href="index.php?page=home">LogIn</a></span>
              <span><a href="index.php?page=parsing">Parsing</a></span>
              <?php if ($_GET['all']) include "view/link.php"; //Link to show all Results?>
            </nav>
            <section>
                <h1>Sed ut perspiciatis</h1>
                <p>
                    Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
                </p>
                <button>Get Stardet Now</button>
            </section>
          </div>
          <div>
              <img src="img/header.gif" alt="header">
          </div>
          
        </header>

        <main>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="js/script.js"></script>