<?php
    include "classes/Tables.php";
    include "classes/DB.php";
    $tables = new Tables;
    $result = $tables -> getTables();
    $n=1;
?>
<table>
    <tr>
        <th>#</th>
        <th>URL</th>
        <th>Tag Name</th>
        <th>Value</th>
        <th>Created Date</th>
    </tr>
    <?php foreach ($result as $value):?>
    <tr>
        <td><?php echo $n++;?></td>
        <td><?php echo $value[url];?> </td>
        <td><?php echo $value[tagName];?> </td>
        <td><?php echo $value[value];?> </td>
        <td><?php echo $value[createdDate];?> </td>
    </tr>
<?php endforeach; ?>

</table>