</main>

        <footer>
            <div>
                <img src="img/facebook-logo.png" srcset="img/facebook-logo@2x.png 2x, img/facebook-logo@3x.png 3x" class="facebook-logo">
                <img src="img/instagram-social-network-logo-of-photo-camera.png" srcset="img/instagram-social-network-logo-of-photo-camera@2x.png 2x, img/instagram-social-network-logo-of-photo-camera@3x.png 3x" class="instagram-social-network-logo-of-photo-camera">
            </div>
            <div><span>Copyright © 2019</span></div>
        </footer>
    </div>
</body>
</html>