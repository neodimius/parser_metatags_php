
            <article>
                <div><img src="img/picture.jpg"
                    srcset="img/picture@2x.jpg 2x,
                            img/picture@3x.jpg 3x"
                    class="picture">
               </div>
               <div>
                   <h1>Finibus Bonorum et Malorum</h1>
                   <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.</p>
               </div>
            </article>

            <article>
                <div>
                    <h1>Lorem Ipsum</h1>
                    <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
                </div>
                <div>
                    <img src="img/picture_2.jpg" srcset="img/picture_2@2x.jpg 2x, img/picture_2@3x.jpg 3x" class="picture">

                </div>
            </article>
        
