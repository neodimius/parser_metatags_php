<?php
class PARSE {
    public function parseURL ($url){
        $patternURL = '~[a-z]+://\S+~';
        $patternIMG = '~(http.*\.)(jpe?g|png|[tg]iff?|svg)~i';
    //get a meta tags from URL
        $tags = get_meta_tags($url); 
    // Get a Title from URL
        $dom = new DOMDocument;
        $dom -> loadHTMLFile($url);
        $titleObj = $dom->getElementsByTagName('title');
        $title = $titleObj -> item(0) -> nodeValue;
        $arr = array();
        $arr['title'] = $title;
        
        foreach ($tags as $key => $value) {
    //*Search Link and Images*/        
            if(preg_match_all($patternIMG, $value, $out))
                $value = "<img src='" . $value ."'>";
            else if(preg_match_all($patternURL, $value, $out))
                $value = "<a href='" . $value ."'>" . $key . "</a>"; 
            $arr[$key] = $value;
        }
        return $arr;
    }
}